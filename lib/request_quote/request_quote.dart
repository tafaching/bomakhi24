import 'dart:convert';

import 'package:community_app/common/custom_button.dart';
import 'package:community_app/common/widget_factory.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:community_app/viewmodel/request_quote_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';

import '../main.dart';

class RequestQuote extends StatefulWidget {
  @override
  RequestQuoteState createState() {
    return new RequestQuoteState();
  }
}

class RequestQuoteState extends State<RequestQuote> {
  TextEditingController moduleRequirementsController =
      new TextEditingController();

  TextEditingController moduleAddressController = new TextEditingController();
  TextEditingController moduleDateController = new TextEditingController();
  TextEditingController moduleCallsController = new TextEditingController();
  TextEditingController moduleAreaController = new TextEditingController();
  TextEditingController moduleEmailController = new TextEditingController();
  TextEditingController moduleLatController = new TextEditingController();
  TextEditingController moduleLngController = new TextEditingController();
  TextEditingController moduleCityController = new TextEditingController();
  TextEditingController moduleSuburbController = new TextEditingController();
  RequestQuoteViewModel propertyViewModel;
  GlobalKey<FormState> _propertyFormKey = new GlobalKey<FormState>();
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  double lat;
  double lng;
  String lati;
  String lngi;
  var addresses;
  var suburb;
  var city;
  SharedPreferencesHelper sharedPreferencesHelper;
  User user;
  String userIdx;
  String x;

  @override
  void initState() {
    propertyViewModel = RequestQuoteViewModel();
    sharedPreferencesHelper = SharedPreferencesHelper();
    user = User();
    getUserId();
    super.initState();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    setState(() {
      user = User().fromMap(jsonDecode(x));
      userIdx = user.id.toString();
      return user;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: new IconButton(
        //   icon:
        //       new Icon(Icons.arrow_back, color: Theme.of(context).accentColor),
        //   onPressed: () {
        //     // Navigator.of(context).push(new MaterialPageRoute(
        //     //   builder: (c) => AdminPropertyList(),
        //     // ));
        //   },
        // ),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Request a Quote',
          style: TextStyle(color: Theme.of(context).accentColor),
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        padding: new EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 5,
            ),
            SizedBox(
              height: 5,
            ),
            Form(
              key: _propertyFormKey,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  validateDescriptionField(
                    moduleRequirementsController,
                    "Input your requirements here",
                    false,
                    (input) => input.trim().length < 3
                        ? 'Name should not be less than 3 characters'
                        : null,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  validateInputValidationField(
                    moduleAreaController,
                    "Area - where you need service",
                    false,
                    (input) => input.trim().length < 2
                        ? 'Field should not be empty'
                        : null,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  createInputField(
                    moduleDateController,
                    "Date you require  service",
                    false,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  validateNumberField(
                    moduleCallsController,
                    "Contact Number",
                    false,
                    (input) => input.trim().length < 2
                        ? 'Field should not be empty'
                        : null,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  validateInputValidationField(
                    moduleEmailController,
                    "Email Address",
                    false,
                    (input) => input.trim().length < 2
                        ? 'Field should not be empty'
                        : null,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      margin: const EdgeInsets.all(15.0),
                      padding: const EdgeInsets.all(3.0),
                      decoration:
                          BoxDecoration(border: Border.all(color: Colors.grey)),
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Click Button find to add area yo we wont  display it for security reasons",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.redAccent),
                          ),
                          FlatButton(
                            color: Theme.of(context).accentColor,
                            onPressed: () async {
                              Prediction p = await PlacesAutocomplete.show(
                                  context: context, apiKey: kGoogleApiKey);
                              displayPrediction(p);
                            },
                            child: Text(
                              'Find address',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          disabledInputField(moduleAddressController,
                              city ?? 'Address', false),
                          SizedBox(
                            height: 10,
                          ),
                          disabledInputField(
                              moduleCityController, city ?? 'City', false),
                          SizedBox(
                            height: 10,
                          ),
                          disabledInputField(moduleSuburbController,
                              suburb ?? 'Suburb', false),
                          SizedBox(
                            height: 10,
                          ),
                          disabledInputField(
                              moduleLatController, lati ?? 'Latitude', false),
                          SizedBox(
                            height: 5,
                          ),
                          disabledInputField(
                              moduleLngController, lngi ?? 'Longitude', false),
                        ],
                      )),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            CustomButton(
              text: "Submit",
              onPressed: () {
                if (_propertyFormKey.currentState.validate()) {}
              },
            )
          ],
        ),
      ),
    );
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      setState(() {
        lat = detail.result.geometry.location.lat;
        lng = detail.result.geometry.location.lng;
      });

      final coordinates = new Coordinates(lat, lng);
      var addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      var first = addresses.first;
      print(
          ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');

      setState(() {
        suburb = first.subLocality;
        city = first.locality;
        lati = lat.toString();
        lngi = lng.toString();
      });
    }
  }
}
