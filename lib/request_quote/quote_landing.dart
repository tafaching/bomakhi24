import 'dart:io';

import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/Home/home.dart';
import 'package:community_app/viewmodel/emergency_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class QuoteLanding extends StatefulWidget {
  @override
  QuoteLandingState createState() {
    return new QuoteLandingState();
  }
}

class QuoteLandingState extends State<QuoteLanding> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    Provider.of<EmergencyViewModel>(context, listen: false)
        .getMembershipByPhone();
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.redAccent),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Home(),
            ));
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Bomakhi Quotes',
          style: Theme.of(context).textTheme.subhead,
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
      ),
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
//          _buildTopBar(),
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Builder(
      builder: (BuildContext context) {
        return Positioned(
          child: Container(
            padding: const EdgeInsets.only(
                left: 0.0, top: 16.0, bottom: 16.0, right: 0.0),
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                  margin: EdgeInsets.symmetric(horizontal: 32, vertical: 20),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 16),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 16.0),
                                child: Text(
                                  "item.name",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18,
                                      color: Colors.grey[700]),
                                ),
                              ),
                            ),

                            // Platform.isIOS
                            //     ? const Text('Continue in iOS view')
                            //     : const Text('Continue in Android view'),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 80.0, right: 6.0),
                                child: Platform.isIOS
                                    ? const Text('Continue in iOS view')
                                    : const Text('Continue in Android view'),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void _showDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text('Are sure you want to delete panic contact?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(HOME_SCREEN);
                },
                child: Text('cancel'),
              ),
              FlatButton(
                onPressed: () {
                  Provider.of<EmergencyViewModel>(context, listen: false)
                      .deleteContactByPhoneNumber();
                  Navigator.of(context).pop();
                },
                child: Text('yes'),
              )
            ],
          );
        });
  }
}
