import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/viewmodel/login_view_model.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> implements LoginListener {
  final _formKey = GlobalKey<FormState>();
  LoginViewModel loginViewModel;
  User _user;
  bool _invisiblePwd = true;
  bool _isLoading = false;
  bool _autoValidate = false;
  String mssg;
  Color color;
  var cntxt;

  @override
  void initState() {
    super.initState();
    loginViewModel = LoginViewModel();
    _user = User();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        return Container(
          color: Colors.black,
          child: ListView(
            children: <Widget>[
              Center(
                  child: _isLoading
                      ? LinearProgressIndicator()
                      : SizedBox(height: 6.0)),
              Padding(
                padding: EdgeInsets.fromLTRB(24.0, 180.0, 24.0, 0.0),
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  elevation: 25.0,
                  child: Builder(
                    builder: (context) => Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 24.0, horizontal: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                IconButton(
                                  padding: EdgeInsets.all(0.0),
                                  iconSize: 26.0,
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black54,
                                  ),
                                  onPressed: () => Navigator.pop(context),
                                ),
                                Text(
                                  ' Login',
                                  style: TextStyle(fontSize: 26.0),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(24.0, 0.0, 24.0, 16.0),
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 28.0,
                                ),
                                TextFormField(
                                  enabled: !this._isLoading,
                                  decoration: decorate(
                                      "email address", Icons.email, false),
                                  validator: (value) {
                                    return validate(value);
                                  },
                                  autovalidate: _autoValidate,
                                  onSaved: (val) => _user.emailAddress = val,
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                TextFormField(
                                  enabled: !this._isLoading,
                                  decoration:
                                      decorate("Password", Icons.lock, true),
                                  validator: (value) {
                                    return validate(value);
                                  },
                                  autovalidate: _autoValidate,
                                  onSaved: (val) =>
                                      setState(() => _user.password = val),
                                  obscureText: this._invisiblePwd,
                                ),
                                SizedBox(
                                  height: 16.0,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    MaterialButton(
                                      child: Text(
                                        'Login',
                                        style: TextStyle(
                                          fontSize: 22.0,
                                        ),
                                      ),
                                      onPressed: () {
                                        if (_formKey.currentState.validate()) {
                                          _formKey.currentState.save();
                                          loginViewModel.login(_user, this);
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      }),
    );
  }

  String validate(value) {
    this._autoValidate = true;
    if (value.isEmpty) {
      return 'Please fill this field';
    }
    return null;
  }

  InputDecoration decorate(String text, IconData icon, bool password) {
    return InputDecoration(
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.teal[100],
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.green,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.grey,
        ),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.red,
        ),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.deepOrange[200],
        ),
      ),
      labelText: text,
      prefixIcon: Icon(icon),
      suffixIcon: password
          ? IconButton(
              color: Colors.black54,
              padding: EdgeInsets.all(1.0),
              icon: this._invisiblePwd
                  ? Icon(Icons.visibility)
                  : Icon(Icons.visibility_off),
              onPressed: () {
                setState(() {
                  this._invisiblePwd = !this._invisiblePwd;
                });
              })
          : null,
    );
  }

  @override
  void onFail() {
    //TODO: TOEAST
  }

  @override
  void onSuccess() {
    Navigator.of(context).pushNamed(HOME_SCREEN);
  }
}
