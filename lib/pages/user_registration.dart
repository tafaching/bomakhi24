import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/common/custom_button.dart';
import 'package:community_app/common/widget_factory.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/pages/sign_in.dart';
import 'package:community_app/viewmodel/city_view_model.dart';
import 'package:community_app/viewmodel/registration_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  RegistrationScreenState createState() {
    return new RegistrationScreenState();
  }
}

class RegistrationScreenState extends State<RegistrationScreen>
    implements RegistrationListener {
  final TextEditingController moduleNameController =
      new TextEditingController();
  final TextEditingController moduleSurnameNameController =
      new TextEditingController();
  final TextEditingController moduleEmailController =
      new TextEditingController();
  final TextEditingController modulePhoneNumberController =
      new TextEditingController();
  final TextEditingController modulePasswordController =
      new TextEditingController();
  final TextEditingController moduleAddressController =
      new TextEditingController();
  final TextEditingController moduleCityController =
      new TextEditingController();

  RegistrationViewModel registrationViewModel;
  String phone;
  String format;
  String _mySelection;

  @override
  void initState() {
    super.initState();
    registrationViewModel = RegistrationViewModel();
  }

  @override
  Widget build(BuildContext context) {
    validateMobile(modulePhoneNumberController.text);
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Login(),
            ));
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'SignUp',
          style: Theme.of(context).textTheme.subhead,
          textAlign: TextAlign.center,
        ),
      ),
      body: new Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        padding: new EdgeInsets.all(20.0),
        child: new Center(
            child: new Form(
          child: new ListView(
            children: <Widget>[
              createInputField(
                moduleNameController,
                "Name",
                false,
              ),
              SizedBox(
                height: 16,
              ),
              createInputField(moduleSurnameNameController, "Surname", false),
              SizedBox(
                height: 16,
              ),
              createEmailField(moduleEmailController, "Email Address", false),
              SizedBox(
                height: 16,
              ),
              createInputField(
                  moduleAddressController, "Street address", false),
              SizedBox(
                height: 16,
              ),
              // todo City  dropdown
              dropDown(context),
              SizedBox(
                height: 16,
              ),
              createPhoneNumberField(
                  modulePhoneNumberController, "073 000 0001", false),
              SizedBox(
                height: 16,
              ),
              createInputField(modulePasswordController, "Password", false),
              CustomButton(
                text: "Post",
                onPressed: () {
                  registrationViewModel.register(
                      User(
                          name: moduleNameController.text,
                          surname: moduleSurnameNameController.text,
                          phoneNumber:
                              validateMobile(modulePhoneNumberController.text),
                          emailAddress: moduleEmailController.text,
                          address: moduleAddressController.text,
                          city: _mySelection,
                          password: modulePasswordController.text),
                      this);
                },
              )
            ],
          ),
        )),
      ),
    );
  }

  String validateMobile(String value) {
    var re = RegExp(r'\d{1}');

    String str = value.replaceFirst(re, "+27");

    if (str.length == 0) {
      return 'Please enter mobile number';
    }
    return str;
  }

  @override
  void onFail() {
    _showDialog(context);
  }

  @override
  void onSuccess() {
    Navigator.of(context).pushNamed(HOME_SCREEN);
  }

  Widget dropDown(BuildContext context) {
    Provider.of<CityViewModel>(context, listen: false).getAllCities();

    return Consumer<CityViewModel>(builder: (context, cityViewModel, child) {
      return FormField(
        builder: (FormFieldState state) {
          return InputDecorator(
            decoration: InputDecoration(
              icon: const Icon(Icons.location_city),
              labelText: 'City',
            ),
            child: new DropdownButtonHideUnderline(
              child: new DropdownButton(
                value: _mySelection,
                isDense: true,
                onChanged: (newVal) {
                  setState(() {
                    _mySelection = newVal;
                    state.didChange(newVal);
                  });
                },
                items: cityViewModel.cityList.map((item) {
                  return new DropdownMenuItem(
                    value: item.id,
                    child: new Text(item.cityName),
                  );
                }).toList(),
              ),
            ),
          );
        },
      );
    });
  }
}

void _showDialog(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('Something went wrong...Please make sure you enter all fields'),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed(LANDING);
              },
              child: Text('cancel'),
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed(USER_REGISTRATION);
              },
              child: Text('Retry'),
            )
          ],
        );
      });
}
