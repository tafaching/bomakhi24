import 'package:flutter/material.dart';

class ZoneSelectionDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ZoneSelectionState();
  }
}

class ZoneSelectionState extends State<ZoneSelectionDialog> {
  List _cities = [
    "Test Equipments",
    "All",
  ];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentZone;
  String _currentCity;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentCity = _dropDownMenuItems[0].value;
    super.initState();
  }

  // here we are creating the list needed for the DropDownButton
  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _cities) {
      // here we are creating the drop down menu items, you can customize the item right here
      // but I'll just use a simple text for this
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
          height: double.infinity,
          child: Column(
            children: <Widget>[
              Text(
                "Zone Selection",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              ),
              new DropdownButton(
                value: _currentZone,
                items: _dropDownMenuItems,
                onChanged: changedDropDownItem,
              ),
              new Row(
                children: <Widget>[
                  RaisedButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    onPressed: submit,
                  ),
                  RaisedButton(
                    child: Text(
                      "Cancel",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.blue,
                    onPressed: () => Navigator.pop(context),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              )
            ],
          )),
    );
  }

  void changedDropDownItem(String selectedZone) {
    print("Selected city $selectedZone, we are going to refresh the UI");
    setState(() {
     // _currentCity = selectedCity;
    });
  }

  void submit() {}
}