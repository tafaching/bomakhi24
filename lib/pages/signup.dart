import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/viewmodel/registration_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> implements RegistrationListener {
  final _formKey = GlobalKey<FormState>();
  RegistrationViewModel registrationViewModel;
  User _user;

  bool _invisiblePwd = true;
  bool _autoValidate = false;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    registrationViewModel = RegistrationViewModel();
    _user = User();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/signup_bg.png"),
                  alignment: Alignment(0.0, -0.9),
                  fit: BoxFit.scaleDown),
            ),
            child: ListView(
              children: <Widget>[
                Center(
                    child: _isLoading
                        ? LinearProgressIndicator()
                        : SizedBox(height: 6.0)),
                Padding(
                  padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    elevation: 5.0,
                    child: Builder(
                      builder: (context) => Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 24.0, horizontal: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  IconButton(
                                    padding: EdgeInsets.all(0.0),
                                    iconSize: 36.0,
                                    icon: Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.black54,
                                    ),
                                    onPressed: () => Navigator.pop(context),
                                  ),
                                  Text(
                                    ' Register',
                                    style: TextStyle(fontSize: 36.0),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  EdgeInsets.fromLTRB(24.0, 0.0, 24.0, 16.0),
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 28.0,
                                  ),
                                  TextFormField(
                                    enabled: !this._isLoading,
                                    decoration: decorate(
                                        "Name", Icons.account_box, false),
                                    validator: (value) {
                                      return validate(value);
                                    },
                                    autovalidate: _autoValidate,
                                    onSaved: (val) => _user.name = val,
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
                                  TextFormField(
                                    enabled: !this._isLoading,
                                    decoration: decorate(
                                        "Surname", Icons.account_circle, false),
                                    validator: (value) {
                                      return validate(value);
                                    },
                                    autovalidate: _autoValidate,
                                    onSaved: (val) => _user.surname = val,
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
                                  TextFormField(
                                    enabled: !this._isLoading,
                                    decoration: decorate(
                                        "email address", Icons.email, false),
                                    validator: (value) {
                                      return validate(value);
                                    },
                                    autovalidate: _autoValidate,
                                    onSaved: (val) => _user.emailAddress = val,
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
                                  TextFormField(
                                    enabled: !this._isLoading,
                                    decoration:
                                        decorate("Address", Icons.home, false),
                                    validator: (value) {
                                      return validate(value);
                                    },
                                    autovalidate: _autoValidate,
                                    onSaved: (val) => _user.address = val,
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),

                                  DropdownButtonFormField(
                                    decoration:decorate(" --select City--",Icons.location_city,false),
                                    validator: (value) {
                                      return validate(value);
                                    },
                                    items: ['Johannesburg', 'Pretoria', 'Other']
                                        .map((gender) => DropdownMenuItem(
                                        value: gender,
                                        child: Text("$gender")))
                                        .toList(),
                                  ),
//                                  TextFormField(
//                                    enabled: !this._isLoading,
//                                    decoration: decorate(
//                                        "city", Icons.location_city, false),
//                                    validator: (value) {
//                                      return validate(value);
//                                    },
//                                    autovalidate: _autoValidate,
//                                    onSaved: (val) => _user.city = val,
//                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
//                                  TextFormField(
//                                    enabled: !this._isLoading,
//                                    decoration: decorate(
//                                        "suburb", Icons.location_on, false),
//                                    validator: (value) {
//                                      return validate(value);
//                                    },
//                                    autovalidate: _autoValidate,
//                                    onSaved: (val) => _user.suburb = val,
//                                  ),
                                  DropdownButtonFormField(
                                    decoration:decorate(" --select Suburb--",Icons.location_city,false),
                                    validator: (value) {
                                      return validate(value);
                                    },
                                    items: ['Cosmo city', 'Randburg', 'Other']
                                        .map((gender) => DropdownMenuItem(
                                            value: gender,
                                            child: Text("$gender")))
                                        .toList(),
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
                                  TextFormField(
                                    enabled: !this._isLoading,
                                    keyboardType: TextInputType.phone,
                                    inputFormatters: <TextInputFormatter>[
                                      WhitelistingTextInputFormatter.digitsOnly,
                                    ],
                                    decoration: decorate(
                                        "+27 73 890 0001", Icons.phone, false),
                                    validator: (value) {
                                      return validatePhone(value);
                                    },
                                    autovalidate: _autoValidate,
                                    onSaved: (val) => _user.phoneNumber = val,
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
                                  TextFormField(
                                    enabled: !this._isLoading,
                                    decoration: decorate(
                                        "Password", Icons.vpn_key, true),
                                    validator: (value) {
                                      return validate(value);
                                    },
                                    autovalidate: _autoValidate,
                                    onSaved: (val) =>
                                        setState(() => _user.password = val),
                                    obscureText: this._invisiblePwd,
                                  ),
                                  SizedBox(
                                    height: 16.0,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      MaterialButton(
                                        child: Text(
                                          'Register',
                                          style: TextStyle(
                                            fontSize: 22.0,
                                          ),
                                        ),
                                        onPressed: () {
                                          if (_formKey.currentState
                                              .validate()) {
                                            _formKey.currentState.save();
                                            registrationViewModel.register(
                                                _user, this);
                                          }
                                        },
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  String validate(value) {
    this._autoValidate = true;
    if (value.isEmpty) {
      return 'Please fill this field';
    }
    return null;
  }

  String validatePhone(value) {
    this._autoValidate = true;
    if (value.isEmpty) {
      return 'please use that format +27 73 000 00001';
    }
    return null;
  }

  InputDecoration decorate(String text, IconData icon, bool password) {
    return InputDecoration(
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.teal[100],
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.green,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.grey,
        ),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.red,
        ),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.deepOrange[200],
        ),
      ),
      labelText: text,
      prefixIcon: Icon(icon),
      suffixIcon: password
          ? IconButton(
              color: Colors.black54,
              padding: EdgeInsets.all(1.0),
              icon: this._invisiblePwd
                  ? Icon(Icons.visibility)
                  : Icon(Icons.visibility_off),
              onPressed: () {
                setState(() {
                  this._invisiblePwd = !this._invisiblePwd;
                });
              })
          : null,
    );
  }

  @override
  void onFail() {
    // TODO: implement onFail
  }

  @override
  void onSuccess() {
    Navigator.of(context).pushNamed(USER_LOGIN);
  }
}
