import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/common/panic_button.dart';
import 'package:community_app/posts/category/category_list_page.dart';
import 'package:community_app/request_quote/quote_landing.dart';
import 'package:community_app/request_quote/request_quote.dart';
import 'package:community_app/viewmodel/category_view_model.dart';
import 'package:community_app/viewmodel/emergency_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'HomeSliderWidget.dart';

class Home extends StatefulWidget {
  @override
  State createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    Provider.of<CategoryViewModel>(context, listen: false).getAllCategories();
    super.initState();
  }

  @override
  Widget _body(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    {
      return Container(
        margin: EdgeInsets.all(0.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(1.0),
        ),
        child: Consumer<CategoryViewModel>(
            builder: (context, productViewModel, child) {
          return GridView.count(
            shrinkWrap: true,
            primary: false,
            crossAxisSpacing: 0,
            mainAxisSpacing: 0,
            crossAxisCount: 3,
            children:
                List.generate(productViewModel.categoryList.length, (index) {
              final item = productViewModel.categoryList[index];

              return Material(
                  child: InkWell(
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(
                    builder: (c) => CategoryListPage(
                      item: item,
                    ),
                  ));
                },
                child: Container(
                    height: 100.0,
                    width: 100.0,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.grey, width: 0.5),
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin:
                              EdgeInsets.only(left: 1.0, right: 1.0, top: 1.0),
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: NetworkImage(
                                  CAT_IMAGE_URL + item.categoryLogo),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        Text(
                          item.categoryName,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ],
                    )),
              ));
            }),
          );
        }),
      );
    }
  }

  Widget buildOptions() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Material(
          child: InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(LIST_COMUNITY);
            },
            child: Column(
              children: <Widget>[
                Container(
                    height: 100.0,
                    width: 100,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.grey, width: 0.5),
                    ),
                    child: Column(
                      children: [
                        Image.asset(
                          'assets/connection.png',
                          fit: BoxFit.contain,
                          height: 60,
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        Text(
                          '${'Group'}',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.subhead,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
        Material(
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(
                builder: (c) => QuoteLanding(),
              ));
            },
            child: Column(
              children: <Widget>[
                Container(
                    height: 100.0,
                    width: 100,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.grey, width: 0.5),
                    ),
                    child: Column(
                      children: [
                        Image.asset(
                          'assets/quotation.png',
                          fit: BoxFit.contain,
                          height: 60,
                        ),
                        Text(
                          '${'Request Quote'}',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.subhead,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
        Material(
          child: InkWell(
            onTap: () {},
            child: Column(
              children: <Widget>[
                Container(
                    height: 100.0,
                    width: 100,
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.grey, width: 0.5),
                    ),
                    child: Column(
                      children: [
                        Image.asset(
                          'assets/rental.png',
                          fit: BoxFit.contain,
                          height: 60,
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        Text(
                          '${'Rental'}',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.subhead,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          HomeSliderWidget(),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: Column(
                children: <Widget>[
                  buildOptions(),
                  GestureDetector(
                    child: Hero(
                      tag: "panic",
                      child: PanicButton(),
                    ),
                    onDoubleTap: () {
                      Provider.of<EmergencyViewModel>(context, listen: false)
                          .familyPanic();
                    },
                  ),
                  Text(
                    "Double tap to send emergency message",
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
            ),
          ),
          _body(context),
        ],
      ),
    );
  }
}
