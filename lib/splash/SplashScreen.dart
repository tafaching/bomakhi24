import 'dart:async';
import 'dart:convert';

import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  SharedPreferencesHelper sharedPreferencesHelper;
  User user;
  String x;
  String emailx;

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    if (x != null) {
      Navigator.of(context).pushNamed(HOME_SCREEN);
    } else {
      Navigator.of(context).pushReplacementNamed(LANDING);
    }
  }

  @override
  void initState() {
    super.initState();
    sharedPreferencesHelper = SharedPreferencesHelper();
    user = User();
    getUserId();
    startTime();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    user = User().fromMap(jsonDecode(x));
    emailx = user.emailAddress.toString();
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset(
                'assets/bomakhi24_white.jpeg',
                fit: BoxFit.contain,
                height: 300,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
