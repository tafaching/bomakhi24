import 'package:community_app/viewmodel/city_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DropDown extends StatefulWidget {
  @override
  DropDownWidget createState() => DropDownWidget();
}

class DropDownWidget extends State<DropDown> {
  String _mySelection;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Hospital Management"),
      ),
      body: dropDown(context),
    );
  }

  Widget dropDown(BuildContext context) {
    Provider.of<CityViewModel>(context, listen: false).getAllCities();

    return Consumer<CityViewModel>(builder: (context, cityViewModel, child) {
      return FormField(
        builder: (FormFieldState state) {
          return InputDecorator(
            decoration: InputDecoration(
              icon: const Icon(Icons.location_city),
              labelText: 'City',
            ),
            child: new DropdownButtonHideUnderline(
              child: new DropdownButton(
                value: _mySelection,
                isDense: true,
                onChanged: (newVal) {
                  setState(() {
                    _mySelection = newVal;
                    state.didChange(newVal);
                  });
                },
                items: cityViewModel.cityList.map((item) {
                  return new DropdownMenuItem(
                    value: item.id,
                    child: new Text(item.cityName),
                  );
                }).toList(),
              ),
            ),
          );
        },
      );
    });
  }
}
