import 'package:community_app/pages/alert_page.dart';
import 'package:community_app/pages/landing_page.dart';
import 'package:community_app/pages/settings_page.dart';
import 'package:community_app/pages/sign_in.dart';

import 'package:community_app/pages/user_registration.dart';
import 'package:community_app/posts/category/product_details.dart';
import 'package:community_app/splash/SplashScreen.dart';
import 'package:community_app/viewmodel/alert_view_model.dart';
import 'package:community_app/viewmodel/builders_view_model.dart';
import 'package:community_app/viewmodel/carpenters_view_model.dart';
import 'package:community_app/viewmodel/category_view_model.dart';
import 'package:community_app/viewmodel/city_view_model.dart';
import 'package:community_app/viewmodel/electrical_view_model.dart';
import 'package:community_app/viewmodel/emergency_view_model.dart';
import 'package:community_app/viewmodel/garden_view_model.dart';
import 'package:community_app/viewmodel/group_view_model.dart';
import 'package:community_app/viewmodel/login_view_model.dart';
import 'package:community_app/viewmodel/maids_view_model.dart';
import 'package:community_app/viewmodel/plumbers_view_model.dart';
import 'package:community_app/viewmodel/post_view_model.dart';
import 'package:community_app/viewmodel/suburb_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

import 'Constant/Constant.dart';
import 'Home/home.dart';
import 'communityGroups/add_emergency_contact.dart';
import 'communityGroups/community_groupList_page.dart';
import 'communityGroups/emergency_alert_contacts.dart';


void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (context) => LoginViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => EmergencyViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => AlertViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => CityViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => CategoryViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => PostViewModel(),
    ),

    ChangeNotifierProvider(
      create: (context) => ElectricalViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => MaidsViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => BuildersViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => GardenViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => CarpentersViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => PlumbersViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => SuburbViewModel(),
    ),
    ChangeNotifierProvider(
      create: (context) => GroupViewModel(),
    )
  ], child: MyApp()));
  setupLocator();
}

GetIt locator = GetIt.instance;
const kGoogleApiKey = "AIzaSyAvTc4ViR7T4c3WeepIRqcxqvwONjAn690";

void setupLocator() {
  locator.registerSingleton(CallProvider());
  locator.registerLazySingleton(() => CallsAndMessagesService());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Community Response',
        theme: ThemeData(
          primarySwatch: Colors.cyan,
          accentColor: Colors.deepOrange,
        ),
        home: new SplashScreen(),
        routes: <String, WidgetBuilder>{
          HOME_SCREEN: (context) => Home(),
          ANIMATED_SPLASH: (context) => SplashScreen(),
          ARLERT: (context) => AlertPage(),
          USER_LOGIN: (context) => Login(),
          SETTINGS: (context) => SettingsPage(),
          USER_REGISTRATION: (context) => RegistrationScreen(),
          LANDING: (context) => OnBoardingActivity(),
          ADD_EMERGENCY_CONTACTS: (context) => EmergencyContact(),

          ALERT_EMERGENCY: (context) => AlertEmergency(),
          LIST_COMUNITY: (context) => CommunityGroupListPage(),
        });
  }
}
