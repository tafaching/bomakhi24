import 'package:community_app/model/base_model.dart';

class Quote with ModelMapper {
  String id;
  String postTitle;
  String postCategoryName;
  String postDescription;
  String suburb;
  String key;
  String phoneNumber;
  String name;
  String email;
  String categoryId;
  String userId;
  String postImageName;
  String postFeaturedImage;

  Quote(
      {this.id,
        this.postTitle,
      this.postCategoryName,
      this.postDescription,
      this.suburb,
      this.key,
      this.phoneNumber,
      this.name,
      this.email,
      this.categoryId,
      this.userId,
      this.postImageName,
      this.postFeaturedImage});

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    postTitle = map['postTitle']?.toString() ?? '';
    postCategoryName = map['postCategoryName']?.toString() ?? '';
    postDescription = map['postDescription']?.toString() ?? '';
    suburb = map['suburb']?.toString() ?? '';
    key = map['key']?.toString() ?? '';
    phoneNumber = map['phoneNumber']?.toString() ?? '';
    name = map['name']?.toString() ?? '';
    email = map['email']?.toString() ?? '';
    categoryId = map['categoryId']?.toString() ?? '';
    userId = map['userId']?.toString() ?? '';
    postImageName = map['postImageName']?.toString() ?? '';
    postFeaturedImage = map['postFeaturedImage']?.toString() ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Quote();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'postTitle': postTitle,
      'postCategoryName': postCategoryName,
      'postDescription': postDescription,
      'suburb': suburb,
      'key': key,
      'phoneNumber': phoneNumber,
      'name': name,
      'email': email,
      'categoryId': categoryId,
      'userId': userId,
      'postImageName': postImageName,
      'postFeaturedImage': postFeaturedImage,
    };
  }
}
