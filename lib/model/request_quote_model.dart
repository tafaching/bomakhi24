import 'package:community_app/model/base_model.dart';

class RequestQuoteModel with ModelMapper {
  String id;
  String requestTitle;
  String requestDescription;
  String postDescription;
  String suburb;
  String lat;
  String phoneNumber;
  String name;
  String email;
  String categoryId;
  String userId;
  String postImageName;
  String postFeaturedImage;

  RequestQuoteModel(
      {this.id,
        this.requestTitle,
      this.requestDescription,
      this.postDescription,
      this.suburb,
      this.lat,
      this.phoneNumber,
      this.name,
      this.email,
      this.categoryId,
      this.userId,
      this.postImageName,
      this.postFeaturedImage});

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    requestTitle = map['postTitle']?.toString() ?? '';
    requestDescription = map['postCategoryName']?.toString() ?? '';
    postDescription = map['postDescription']?.toString() ?? '';
    suburb = map['suburb']?.toString() ?? '';
    lat = map['key']?.toString() ?? '';
    phoneNumber = map['phoneNumber']?.toString() ?? '';
    name = map['name']?.toString() ?? '';
    email = map['email']?.toString() ?? '';
    categoryId = map['categoryId']?.toString() ?? '';
    userId = map['userId']?.toString() ?? '';
    postImageName = map['postImageName']?.toString() ?? '';
    postFeaturedImage = map['postFeaturedImage']?.toString() ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return RequestQuoteModel();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'postTitle': requestTitle,
      'postCategoryName': requestDescription,
      'postDescription': postDescription,
      'suburb': suburb,
      'key': lat,
      'phoneNumber': phoneNumber,
      'name': name,
      'email': email,
      'categoryId': categoryId,
      'userId': userId,
      'postImageName': postImageName,
      'postFeaturedImage': postFeaturedImage,
    };
  }
}
