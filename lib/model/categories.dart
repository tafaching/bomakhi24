import 'package:community_app/model/base_model.dart';

class Categories with ModelMapper {
  String id;
  String categoryName;
  String categoryLogo;
  String rating;

  Categories({
    this.id,
    this.categoryName,
    this.categoryLogo,
    this.rating,
  });

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    categoryName = map['categoryName']?.toString() ?? '';
    categoryLogo = map['categoryLogo']?.toString() ?? '';
    rating = map['rating']?.toString() ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Categories();
  }

  Map toJson() {
    return {
      'id': id,
      'categoryName': categoryName,
      'categoryLogo': categoryLogo,
      'rating': rating,
    };
  }
}
