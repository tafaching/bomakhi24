import 'package:community_app/model/base_model.dart';

class Suburb with ModelMapper {
  String id;
  String cityName;
  String suburbName;
  String cityId;

  Suburb({
    this.id,
    this.cityName,
    this.suburbName,
    this.cityId,
  });

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    cityName = map['cityName']?.toString() ?? '';
    suburbName = map['suburbName']?.toString() ?? '';
    cityId = map['cityId']?.toString() ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Suburb();
  }

  Map toJson() {
    return {
      'id': id,
      'cityName': cityName,
      'suburbName': suburbName,
      'cityId': cityId,
    };
  }
}
