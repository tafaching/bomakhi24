import 'package:community_app/model/base_model.dart';

class Group with ModelMapper {
  String id;
  String groupName;
  String suburbId;

  Group({
    this.id,
    this.groupName,
    this.suburbId,
  });

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    groupName = map['groupName']?.toString() ?? '';
    suburbId = map['suburbId']?.toString() ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Group();
  }

  Map toJson() {
    return {
      'id': id,
      'groupName': groupName,
      'suburbId': suburbId,

    };
  }
}
