import 'base_model.dart';

class Wish with ModelMapper {
  int id;
  String name;
  String price;

  String user_id;
  String photo;
  String product_id;

  Wish({
    this.id,
    this.name,
    this.price,
    this.user_id,
    this.photo,
    this.product_id,
  });

  @override
  ModelMapper fromMap(map) {
    id = map['id'];
    name = map['name'];
    price = map['price'];
    user_id = map['user_id'];
    photo = map['photo'];
    product_id = map['product_id'];

    return this;
  }

  @override
  ModelMapper newInstance() {
    return Wish();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'user_id': user_id,
      'photo': photo,
      'product_id': product_id,
    };
  }
}
