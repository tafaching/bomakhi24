class Slider {
  String image;
  String button;
  String description;

  Slider({this.image, this.button, this.description});
}

class SliderList {
  List<Slider> _list;

  List<Slider> get list => _list;

  SliderList() {
    _list = [
      new Slider(
          image: 'assets/slider1.jpg',
          button: 'Explore',
          description: 'A room without books is like a body without a soul.'),
      new Slider(
          image: 'assets/slider2.jpg', button: 'Explore', description: 'Be yourself, everyone else is already taken.'),
      new Slider(image: 'assets/slider3.jpg', button: 'Explore', description: 'So many books, so little time.'),
    ];
  }
}
