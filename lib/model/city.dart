import 'package:community_app/model/base_model.dart';

class Categories with ModelMapper {
  String id;
  String cityName;

  Categories({
    this.id,
    this.cityName,
  });

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    cityName = map['cityName']?.toString() ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Categories();
  }

  Map toJson() {
    return {
      'id': id,
      'cityName': cityName,
    };
  }
}
