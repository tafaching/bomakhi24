import 'package:community_app/model/base_model.dart';

class Quote with ModelMapper {
  String requirements;
  String area;
  String date;
  String phone;
  String email;
  String name;
  String id;

  Quote({
    this.id,
    this.area,
    this.date,
    this.phone,
    this.email,
    this.requirements,
    this.name,
  });

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';

    requirements = map['requirements'] ?? '';
    name = map['name'] ?? '';
    email = map['email'] ?? '';
    area = map['area'] ?? '';
    date = map['date'] ?? '';
    phone = map['phone'] ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Quote();
  }

  Map toJson() {
    return {
      'id': id,
      'requirements': requirements,
      'name': name,
      'area': area,
      'date': date,
      'phone': phone,
      'email': email
    };
  }
}
