import 'package:community_app/model/base_model.dart';

class Alert with ModelMapper {
  String phoneNumber;
  String name;
  int id;

  Alert({this.phoneNumber, this.name, this.id});

  @override
  ModelMapper fromMap(map) {
    phoneNumber = map['phoneNumber']?.toString() ?? '';
    name = map['name']?.toString() ?? '';
    id = map['id'] ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Alert();
  }

  @override
  Map toJson() {
    return {'phoneNumber': phoneNumber, 'name': name, 'id': id};
  }
}

