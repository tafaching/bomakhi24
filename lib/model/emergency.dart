import 'package:community_app/model/base_model.dart';

class Emergency with ModelMapper {
  String phoneNumber;
  String userId;
  String groupId;
  String suburbId;
  String groupName;
  String name;
  String accepted;
  String adminPhoneNumber;
  String id;

  Emergency(
      {this.id,
      this.userId,
      this.groupId,
      this.suburbId,
      this.groupName,
      this.phoneNumber,
      this.name,
      this.accepted,
      this.adminPhoneNumber});

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    adminPhoneNumber = map['adminPhoneNumber'] ?? '';
    phoneNumber = map['phoneNumber'] ?? '';
    name = map['name'] ?? '';
    accepted = map['accepted'] ?? '';
    groupName = map['groupName'] ?? '';
    userId = map['userId'] ?? '';
    groupId = map['groupId'] ?? '';
    suburbId = map['suburbId'] ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Emergency();
  }

  Map toJson() {
    return {
      'id': id,
      'adminPhoneNumber': adminPhoneNumber,
      'phoneNumber': phoneNumber,
      'name': name,
      'userId': userId,
      'accepted': accepted,
      'groupId': groupId,
      'suburbId': suburbId,
      'groupName': groupName
    };
  }
}
