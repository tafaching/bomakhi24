import 'package:community_app/model/base_model.dart';

class JsonEncoderMapper<T extends ModelMapper> {
  List<T> mapToLis(T classType, dynamic response) {
    List<T> responseList = [];
    var json = response as List;
    for (var item in json) {
      var map = item as Map;
      responseList.add(classType.newInstance().fromMap(map));
    }
    return responseList;
  }
}
