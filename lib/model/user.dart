import 'package:community_app/model/base_model.dart';

class User with ModelMapper {
  String id;
  String name;
  String address;
  String city;
  String suburb;
  String surname;
  String emailAddress;
  String phoneNumber;
  String password;

  User(
      {this.id,
      this.name,
      this.surname,
      this.address,
      this.city,
      this.suburb,
      this.emailAddress,
      this.phoneNumber,
      this.password});

  @override
  ModelMapper fromMap(map) {
    id = map['id']?.toString() ?? '';
    name = map['name']?.toString() ?? '';
    surname = map['surname']?.toString() ?? '';
    emailAddress = map['emailAddress']?.toString() ?? '';
    password = map['password']?.toString() ?? '';
    phoneNumber = map['phoneNumber']?.toString() ?? '';

    address = map['address']?.toString() ?? '';
    city = map['city']?.toString() ?? '';
    suburb = map['suburb']?.toString() ?? '';

    return this;
  }

  @override
  ModelMapper newInstance() {
    return User();
  }

  Map toJson() {
    return {
      'id': id,
      'name': name,
      'surname': surname,
      'emailAddress': emailAddress,
      'phoneNumber': phoneNumber,
      'address': address,
      'city': city,
      'suburb': suburb,
      'password': password
    };
  }
}
