import 'package:dio/dio.dart';

class HeadersInterceptor extends Interceptor {
  @override
  Future onRequest(RequestOptions options) {
    options.headers
        .update('ExampleHeader', (_) => 'data', ifAbsent: () => 'data');
    options.responseType = ResponseType.json;

    return super.onRequest(options);
  }
}
