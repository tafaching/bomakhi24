import 'package:dio/dio.dart';

import 'dio_client.dart';

Future getData(String endPoint, {Map params}) async {
  Response response =
      await DioClient.getDioClient().get(endPoint, queryParameters: params);
  return response?.data;
}

Future deleteData(String endPoint, {Map params}) async {
  Response response =
  await DioClient.getDioClient().delete(endPoint, queryParameters: params);
  return response;
}

Future postData(String endPoint, dynamic requestData, {Map params}) async {
  Response response = await DioClient.getDioClient()
      .post(endPoint, data: requestData, queryParameters: params,);
  return response;
}

Future postDa(String endPoint, {Map params}) async {
  Response response = await DioClient.getDioClient()
      .post(endPoint,queryParameters: params);
  return response;
}


Future putData(String endPoint, dynamic requestData, {Map params}) async {
  Response response = await DioClient.getDioClient()
      .put(endPoint, data: requestData, queryParameters: params);
  return response;
}