import 'package:dio/dio.dart';

import 'headers_interceptor.dart';

class DioClient {
  static Dio getDioClient() {
    return Dio()
 ..options = BaseOptions(baseUrl: 'http://192.168.8.100:8888/community_response/public/')
     // ..options =BaseOptions(baseUrl: 'http://192.168.8.100:8888/community_response/public/')
 //..options = BaseOptions(baseUrl: 'http://g40property.co.za/community_response/public/')
      ..interceptors.add(HeadersInterceptor())
      ..interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
  }
}
