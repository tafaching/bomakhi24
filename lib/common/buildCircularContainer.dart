import 'package:flutter/material.dart';

Widget buildCircularContainer(String title, AssetImage _icon){
  final baseRedColor = Colors.red[900];
  return Column(
    children: <Widget>[
      Container(
        padding: EdgeInsets.all(22),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(32)),
            boxShadow: [
              BoxShadow(
                  blurRadius: 5,
                  color: Colors.black26
              )
            ]
        ),
        child: Center(
          child: ImageIcon(_icon, color: baseRedColor,),
        ),
      ),
      Text(title,
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white),
      )
    ],
  );
}