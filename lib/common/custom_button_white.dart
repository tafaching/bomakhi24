import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButtonWhite extends StatelessWidget {
  CustomButtonWhite({@required this.onPressed, @required this.text});

  final GestureTapCallback onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 40.0),
    child: MaterialButton(
      color: Colors.white,
      highlightColor: Colors.white,
      splashColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(
            vertical: 10.0, horizontal: 52.0),
        child: Text(
          text,
          style: TextStyle(
              color: Colors.black,
              fontSize: 20,
              fontFamily: "WorkSansBold"),
        ),
      ),
      onPressed: onPressed,

    ));
  }
}

