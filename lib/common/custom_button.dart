import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  CustomButton({@required this.onPressed, @required this.text});

  final GestureTapCallback onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 40.0),
    child: MaterialButton(
      color: Colors.black87,
      highlightColor: Colors.black87,
      splashColor: Colors.black87,
      child: Padding(
        padding: const EdgeInsets.symmetric(
            vertical: 10.0, horizontal: 52.0),
        child: Text(
          text,
          style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: "WorkSansBold"),
        ),
      ),
      onPressed: onPressed,

    ));
  }
}