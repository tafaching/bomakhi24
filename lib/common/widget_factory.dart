import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

createInputFieldWithIcon(
    IconData icon, var controller, var label, bool obscure) {
  return new Container(
      decoration: BoxDecoration(
          color: Colors.white
      ),
      padding: const EdgeInsets.only(top: 10.0,bottom: 20),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          keyboardType: TextInputType.text,
          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              icon: new Icon(icon),
              border: OutlineInputBorder()
          )
      )
  );
}

createTextView(var textControl) {
  return new Container(
      decoration: BoxDecoration(
          color: Colors.white
      ),
      padding: const EdgeInsets.all(10.0), child: Material(child: textControl));
}

createEditBarSearch(var controller, var hint) {
  return Container(
    padding: EdgeInsets.only(top: 10, bottom: 10, right: 15.0),
    child: Material(
      borderRadius: BorderRadius.circular(2.0),
      color: Colors.deepPurple,
      child: TextFormField(
          controller: controller,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              prefixIcon: Icon(Icons.search, color: Colors.white, size: 30.0),
              contentPadding: EdgeInsets.all(10),
              hintText: hint,
              hintStyle: TextStyle(
                color: Colors.grey,
              )
          )
      ),
    ),
  );
}

createInputField(
    var controller, var label, bool obscure) {
  return new Container(
      decoration: BoxDecoration(
          color: Colors.white
      ),

      child: new TextFormField(

          controller: controller,
          obscureText: obscure,
          style: TextStyle(
              color: Colors.black
          ),
          keyboardType: TextInputType.text,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              fillColor: Colors.grey,
              focusColor:Colors.grey,
              labelStyle: TextStyle(
                  color: Colors.grey
              ),
              hintStyle: TextStyle(
                color: Colors.grey
              ),

             )));
}

createEmailField(
    var controller, var label, bool obscure) {
  return new Container(
      decoration: BoxDecoration(
          color: Colors.white
      ),

      child: new TextFormField(
          controller: controller,
          obscureText: obscure,

          style: TextStyle(
              color: Colors.black
          ),

          keyboardType: TextInputType.emailAddress,

          // Use email input type for emails.
          decoration: new InputDecoration(

              labelText: label,
              focusColor:Colors.grey,
              fillColor: Colors.grey,
              labelStyle: TextStyle(
                  color: Colors.grey
              ),
              hintStyle: TextStyle(
                  color: Colors.grey
              ),
              )));
}
createPhoneNumberField(
    var controller, var label, bool obscure) {
  return new Container(
      decoration: BoxDecoration(
        color: Colors.white
      ),

      child: new TextFormField(
          controller: controller,
          obscureText: obscure,

          style: TextStyle(
              color: Colors.black,
          ),
          keyboardType: TextInputType.number,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              focusColor:Colors.grey,
              fillColor: Colors.grey,
              labelStyle: TextStyle(
                  color: Colors.grey
              ),
              hintStyle: TextStyle(
                  color: Colors.grey
              ),

             )));
}

validateInputValidationField(
    var controller, var label, bool obscure, var validator) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          style: TextStyle(color: Colors.black),
          keyboardType: TextInputType.text,
          enabled: true,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              fillColor: Colors.grey,
              focusColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

validateDescriptionField(var controller, var label, bool obscure, var validator) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          style: TextStyle(color: Colors.black),
          maxLines: null,
          keyboardType: TextInputType.multiline,
          textInputAction: TextInputAction.newline,
          enabled: true,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              fillColor: Colors.grey,
              focusColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

validateEmailField(
    var controller, var label, bool obscure, var validator, var onSaved) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          onSaved: onSaved,
          style: TextStyle(color: Colors.black),
          keyboardType: TextInputType.emailAddress,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              focusColor: Colors.grey,
              fillColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}

validateNumberField(var controller, var label, bool obscure, var validator) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          validator: validator,
          style: TextStyle(
            color: Colors.black,
          ),
          keyboardType: TextInputType.number,

          // Use email input type for emails.
          decoration: new InputDecoration(
              labelText: label,
              focusColor: Colors.grey,
              fillColor: Colors.grey,
              labelStyle: TextStyle(color: Colors.grey),
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: OutlineInputBorder(
                borderSide: new BorderSide(color: Colors.grey),
              ))));
}
disabledInputField(var controller, var label, bool obscure) {
  return new Container(
      decoration: BoxDecoration(color: Colors.white),
      child: new TextFormField(
          controller: controller,
          obscureText: obscure,
          style: TextStyle(color: Colors.black),
          keyboardType: TextInputType.text,
          enabled: false,

          // Use email input type for emails.
          decoration: new InputDecoration(
            labelText: label,
            fillColor: Colors.grey,
            focusColor: Colors.grey,
            labelStyle: TextStyle(color: Colors.grey),
            hintStyle: TextStyle(color: Colors.grey),
          )));
}