import 'package:flutter/material.dart';


String appName = "Agu";

Color colorGradientTop = Colors.black54;
Color colorGradientBottom = Colors.black87;

Gradient appGradient =
    LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter,
    colors: [colorGradientTop, colorGradientBottom,], stops: [0,0.7]);