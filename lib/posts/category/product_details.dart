import 'dart:convert';

import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/model/post.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/model/wish.dart';
import 'package:community_app/pages/sign_in.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:community_app/viewmodel/wish_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../main.dart';

class ProductDetails extends StatefulWidget {
  final Quote item;

  ProductDetails({Key key, this.item}) : super(key: key);

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final CallProvider _service = locator<CallProvider>();
  bool favourite = false;
  Color color = Colors.grey;
  SharedPreferencesHelper sharedPreferencesHelper;

  WishViewModel wishViewModel;
  User user;
  int userIdx;
  String x;
  int cartItem = 0;
  String namex;
  String photox;
  String emailx;
  String typex;
  String typeIdx;
  String passwordx;
  String phoneNumberx;

  @override
  void initState() {
    wishViewModel = WishViewModel();
    sharedPreferencesHelper = SharedPreferencesHelper();
    user = User();
    getUserId();
    super.initState();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    setState(() {
      user = User().fromMap(jsonDecode(x));
      userIdx = user.id as int;
      return user;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget description = Padding(
      padding: const EdgeInsets.all(24.0),
      child: Text(
        widget.item.postDescription,
        maxLines: 25,
        semanticsLabel: '...',
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w300,
            fontStyle: FontStyle.normal,
            fontSize: 16.0),
      ),
    );

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: Text(
            widget.item.postTitle ?? '',
            style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontFamily: "Montserrat",
                fontSize: 18.0),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                details(context),

                Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: 150,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(4.0),
                          border: Border.all(color: Colors.black, width: 0.5),
                        ),
                        child: Center(
                          child: new Text("Details",
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                  fontSize: 16.0)),
                        ),
                      ),
                      RawMaterialButton(
                        onPressed: () {
                          if (x == null) {
                            Navigator.of(context).push(new MaterialPageRoute(
                              builder: (c) => Login(),
                            ));
                          } else {
                            setState(() {
                              if (!favourite) {
                                favourite = true;
                                color = Colors.red;
                                wishViewModel.addToWish(
                                  Wish(
                                    product_id: widget.item.id.toString(),
                                    name: widget.item.name,
                                    user_id: userIdx.toString(),
                                  ),
                                );
                                _addWish(context);
                              } else {
                                favourite = false;
                                color = Colors.grey;
                                _removeWish(context);
                              }
                            });
                          }
                        },
                        constraints:
                            const BoxConstraints(minWidth: 45, minHeight: 45),
                        child: Icon(
                          (!favourite)
                              ? FontAwesomeIcons.heart
                              : FontAwesomeIcons.solidHeart,
                          color: color,
                        ),
                        elevation: 0.0,
                        shape: CircleBorder(),
                        fillColor: Colors.white,
                      ),
                    ],
                  ),
                ),
                description,

                Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 30),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: 150,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(4.0),
                              border:
                                  Border.all(color: Colors.black, width: 0.5),
                            ),
                            child: Center(
                              child: new Text("Other",
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300,
                                      fontStyle: FontStyle.normal,
                                      fontSize: 16.0)),
                            ),
                          ),
                        ])),
                // BestSellerGrid()
//                MoreProducts()
              ],
            ),
          ),
        ));
  }

  _addWish(BuildContext context) {
    final snackBar = SnackBar(content: Text('Added to favourite'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _removeWish(BuildContext context) {
    final snackBar = SnackBar(content: Text('Remove from favourite'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _noWhatsAppNumber(BuildContext context) {
    final snackBar = SnackBar(
        content: Text(
      'Service provider has no whatsApp Number, Please call or email them',
      style: TextStyle(
          color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),
    ));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Widget details(BuildContext context) {
    return Container(
      height: 250,
      child: Stack(
        children: <Widget>[
          Positioned(
            left: 16.0,
            child: Container(
                //TODO Adjust image  height
                margin: EdgeInsets.all(6.0),
                height: 200.0,
                child: Image.network(
                  CATEGORY_IMAGE_URL + widget.item.postFeaturedImage,
                  fit: BoxFit.cover,
                )),
          ),
          Positioned(
            right: 0.0,
            child: Container(
              height: 180,
              width: 300,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                    onTap: () async {
                      _service.call(widget.item.phoneNumber);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0))),
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Center(
                        child: Text(
                          'Call',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      if (widget.item.phoneNumber == null) {
                        FlutterOpenWhatsapp.sendSingleMessage(
                            widget.item.phoneNumber, "Hello");
                      } else {
                        _noWhatsAppNumber(context);
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0))),
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Center(
                        child: Text(
                          'Whatsapp',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (x == null) {
                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (c) => Login(),
                        ));
                      } else {
                        _service.sendEmail(widget.item.email);
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.5,
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0))),
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Center(
                        child: Text(
                          'Email',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

// Bottom Sheet for Product Description Ends Here
}

class CallProvider {
  void call(String number) => launch("tel:$number");

  void sendSms(String number) => launch("sms:$number");

  void sendEmail(String email) => launch("mailto:$email");
}
