import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/Home/home.dart';
import 'package:community_app/model/categories.dart';
import 'package:community_app/posts/category/product_details.dart';
import 'package:community_app/viewmodel/electrical_view_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

import '../postAd.dart';

class CategoryListPage extends StatefulWidget {
  final Categories item;

  CategoryListPage({Key key, this.item}) : super(key: key);

  @override
  State createState() {
    return _CategoryListPageState();
  }
}

class _CategoryListPageState extends State<CategoryListPage> {
  String layout = 'grid';
  Categories categories;

  @override
  void initState() {
    Provider.of<ElectricalViewModel>(context, listen: false)
        .getElectrical(widget.item.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Home(),
            ));
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.grey,
        onPressed: () {
          Navigator.of(context).push(new MaterialPageRoute(
            builder: (c) => PostAdScreen(),
          ));
        },
        label: Text(
          'Post Ad',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Wrap(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 10),
              child: ListTile(
                dense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 0),
                title: Text(
                  '${widget.item.categoryName}',
                  overflow: TextOverflow.fade,
                  softWrap: false,
                  style: Theme
                      .of(context)
                      .textTheme
                      .display1,
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    IconButton(
                      onPressed: () {
                        setState(() {
                          this.layout = 'list';
                        });
                      },
                      icon: Icon(
                        Icons.format_list_bulleted,
                        color: this.layout == 'list'
                            ? Colors.black
                            : Colors.black26,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        setState(() {
                          this.layout = 'grid';
                        });
                      },
                      icon: Icon(
                        Icons.apps,
                        color: this.layout == 'grid'
                            ? Colors.black
                            : Colors.black26,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Consumer<ElectricalViewModel>(
              builder: (context, electricalViewModel, child) {
                return Offstage(
                  offstage: this.layout != 'list',
                  child: ListView.separated(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    primary: false,
                    itemCount: electricalViewModel.electricalList.length,
                    separatorBuilder: (context, index) {
                      final item = electricalViewModel.electricalList[index];
                      return SizedBox(height: 10);
                    },
                    itemBuilder: (context, index) {
                      final item = electricalViewModel.electricalList[index];
                      return InkWell(
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                            builder: (c) => ProductDetails(item: item),
                          ));
                        },
                        child: Container(
                          height: 60,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                                color: Colors.grey, width: 0.5),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 5.0,bottom: 1.0,top: 1.0),

                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                width: 100,
                                child: Image.network(CATEGORY_IMAGE_URL +
                                    item.postFeaturedImage,fit: BoxFit.cover,),
                              ),
                              SizedBox(width: 15),
                              Flexible(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            item.postTitle,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 3,
                                            style: Theme
                                                .of(context)
                                                .textTheme
                                                .subhead,
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            ),
            Consumer<ElectricalViewModel>(
              builder: (context, electricalViewModel, child) {
                return Offstage(
                  offstage: this.layout != 'grid',
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: new StaggeredGridView.countBuilder(
                      primary: false,
                      shrinkWrap: true,
                      crossAxisCount: 4,
                      itemCount: electricalViewModel.electricalList.length,
                      itemBuilder: (BuildContext context, int index) {
                        final item = electricalViewModel.electricalList[index];

                        return InkWell(
                            onTap: () {
                              Navigator.of(context).push(new MaterialPageRoute(
                                builder: (c) => ProductDetails(item: item),
                              ));
                            },


                            child: Container(
                                height: 130.0,
                                width: 100,
                                margin: EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0),
                                  border: Border.all(
                                      color: Colors.grey, width: 0.5),
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 1.0,right: 1.0,top: 1.0),
                                      height: 80,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(CATEGORY_IMAGE_URL +
                                              item.postFeaturedImage),
                                          colorFilter: ColorFilter.mode(
                                            Colors.black.withOpacity(0.2),
                                            BlendMode.hardLight,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      item.postTitle,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .subhead,
                                    ),
                                  ],
                                )),

                        );
                      },
                      staggeredTileBuilder: (int index) =>
                      new StaggeredTile.fit(2),
                      mainAxisSpacing: 15.0,
                      crossAxisSpacing: 15.0,
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
