// import 'package:community_app/Constant/Constant.dart';
// import 'package:community_app/model/post.dart';
// import 'package:carousel_pro/carousel_pro.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// import '../main.dart';
//
// class PostProductDetail extends StatefulWidget {
//   final Post item;
//
//   PostProductDetail({Key key, this.item}) : super(key: key);
//
//   @override
//   _PostProductDetailState createState() => _PostProductDetailState();
// }
//
// class _PostProductDetailState extends State<PostProductDetail> {
//   final CallProvider _service = locator<CallProvider>();
//   bool favourite = false;
//   Color color = Colors.grey;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double width = MediaQuery.of(context).size.width;
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.item.name),
//         titleSpacing: 0.0,
//         backgroundColor: Colors.white,
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(
//               Icons.favorite,
//               color: Colors.black,
//             ),
//             onPressed: () {},
//           ),
//         ],
//       ),
//       backgroundColor: const Color(0xFFF1F3F6),
//       body: detail(context),
//       bottomNavigationBar: Material(
//         elevation: 5.0,
//         child: Container(
//           color: Colors.white,
//           width: width,
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: <Widget>[
//               ButtonTheme(
//                 minWidth: ((width) / 2),
//                 height: 50.0,
//                 child: RaisedButton(
//                   child: Icon(
//                     Icons.email,
//                     color: Colors.redAccent,
//                   ),
//                   onPressed: () {
//                     _service.sendEmail(widget.item.email);
//                   },
//                   color: Colors.white,
//                 ),
//               ),
//               ButtonTheme(
//                 // minWidth: ((width - 60.0) / 2),
//                 minWidth: ((width) / 2),
//                 height: 50.0,
//                 child: RaisedButton(
//                   child: Icon(
//                     Icons.call,
//                     color: Colors.green,
//                   ),
//                   onPressed: () {
//                     _service.call(widget.item.phoneNumber);
//                   },
//                   color: Colors.white,
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget detail(BuildContext context) {
//     double height = MediaQuery.of(context).size.height;
//     return ListView(
//       shrinkWrap: true,
//       children: <Widget>[
//         // Slider and Add to Wishlist Code Starts Here
//         Stack(
//           children: <Widget>[
//             Container(
//               padding: EdgeInsets.only(top: 8.0),
//               color: Colors.white,
//               child: Hero(
//                 tag: '${widget.item.name}',
//                 child: SizedBox(
//                   height: (height / 2.0),
//                   child: Carousel(
//                     images: [
//                       Image.network(
//                           CATEGORY_IMAGE_URL + widget.item.postFeaturedImage)
//                     ],
//                     dotSize: 5.0,
//                     dotSpacing: 15.0,
//                     dotColor: Colors.grey,
//                     indicatorBgPadding: 5.0,
//                     dotBgColor: Colors.purple.withOpacity(0.0),
//                     boxFit: BoxFit.fitHeight,
//                     animationCurve: Curves.decelerate,
//                     dotIncreasedColor: Colors.blue,
//                     overlayShadow: true,
//                     overlayShadowColors: Colors.white,
//                     overlayShadowSize: 0.7,
//                   ),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 20.0,
//               right: 20.0,
//               child: FloatingActionButton(
//                 backgroundColor: Colors.white,
//                 elevation: 3.0,
//                 onPressed: () {
//                   setState(() {
//                     if (!favourite) {
//                       favourite = true;
//                       color = Colors.red;
//
//                       Scaffold.of(context).showSnackBar(
//                           SnackBar(content: Text("Added to Wishlist")));
//                     } else {
//                       favourite = false;
//                       color = Colors.grey;
//                       Scaffold.of(context).showSnackBar(
//                           SnackBar(content: Text("Remove from Wishlist")));
//                     }
//                   });
//                 },
//                 child: Icon(
//                   (!favourite)
//                       ? FontAwesomeIcons.heart
//                       : FontAwesomeIcons.solidHeart,
//                   color: color,
//                 ),
//               ),
//             ),
//           ],
//         ),
//         Container(
//             color: Colors.white,
//             child: SizedBox(
//               height: 8.0,
//             )),
//         Divider(
//           height: 1.0,
//         ),
//
//         Container(
//           color: Colors.white,
//           padding: EdgeInsets.all(10.0),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               // Product Title Start Here
//               Text(
//                 '${widget.item.postTitle}',
//                 style: TextStyle(
//                   fontSize: 18.0,
//                   fontWeight: FontWeight.bold,
//                 ),
//                 textAlign: TextAlign.start,
//               ),
//               // Product Title End Here
//
//               // Special Price badge Start Here
//               Container(
//                 margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
//                 padding: EdgeInsets.all(3.0),
//                 decoration: BoxDecoration(
//                   color: Colors.grey[200],
//                   borderRadius: BorderRadius.circular(5.0),
//                 ),
//                 child: Text(
//                   '${8} years experience',
//                   style: TextStyle(color: Colors.red[800], fontSize: 12.0),
//                 ),
//               ),
//               // Rating Row Ends Here
//             ],
//           ),
//         ),
//
//         Container(
//           padding: EdgeInsets.all(10.0),
//           margin: EdgeInsets.only(top: 5.0),
//           color: Colors.white,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Text(
//                 'Description',
//                 style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
//               ),
//               SizedBox(
//                 height: 8.0,
//               ),
//               Text(
//                 widget.item.postDescription,
//                 style: TextStyle(fontSize: 14.0, height: 1.45),
//                 overflow: TextOverflow.ellipsis,
//                 maxLines: 5,
//                 textAlign: TextAlign.start,
//               ),
//               SizedBox(height: 5.0),
//               Divider(
//                 height: 1.0,
//               ),
//               InkWell(
//                 child: Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       Text(
//                         'View More',
//                         style: TextStyle(
//                             color: Theme.of(context).primaryColor,
//                             fontSize: 14.0,
//                             fontWeight: FontWeight.bold),
//                       ),
//                     ],
//                   ),
//                 ),
//                 onTap: () {
//                   _productDescriptionModalBottomSheet(context);
//                 },
//               ),
//               Divider(
//                 height: 1.0,
//               ),
//             ],
//           ),
//         ),
//         // Product Description Ends Here
//
//         // Similar Product Starts Here
//         Container(
//           padding: EdgeInsets.all(10.0),
//           margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
//           color: Colors.white,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Text(
//                 'Similar Adds',
//                 style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
//               ),
//               SizedBox(
//                 height: 8.0,
//               ),
//               // GetSimilarProducts(),
//             ],
//           ),
//         ),
//         // Similar Product Ends Here
//       ],
//     );
//   }
//
//   // Bottom Sheet for Product Description Starts Here
//   void _productDescriptionModalBottomSheet(context) {
//     showModalBottomSheet(
//         context: context,
//         builder: (BuildContext bc) {
//           return Container(
//             child: new Wrap(
//               children: <Widget>[
//                 Container(
//                   child: Container(
//                     margin: EdgeInsets.all(8.0),
//                     child: Column(
//                       children: <Widget>[
//                         Text(
//                           'Description',
//                           style: TextStyle(
//                               fontSize: 16.0, fontWeight: FontWeight.bold),
//                         ),
//                         SizedBox(
//                           height: 8.0,
//                         ),
//                         Divider(
//                           height: 1.0,
//                         ),
//                         SizedBox(
//                           height: 8.0,
//                         ),
//                         Text(
//                           widget.item.postDescription,
//                           style: TextStyle(fontSize: 14.0, height: 1.45),
//                           // overflow: TextOverflow.ellipsis,
//                           // maxLines: 5,
//                           textAlign: TextAlign.start,
//                         ),
//                         SizedBox(
//                           height: 8.0,
//                         ),
//                       ],
//                     ),
//                   ),
//                 )
//               ],
//             ),
//           );
//         });
//   }
// }
//
// class CallProvider {
//   void call(String number) => launch("tel:$number");
//
//   void sendSms(String number) => launch("sms:$number");
//
//   void sendEmail(String email) => launch("mailto:$email");
// }
