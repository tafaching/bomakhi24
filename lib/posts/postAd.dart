import 'dart:convert';
import 'dart:io';

import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/common/custom_button.dart';
import 'package:community_app/common/custom_button_white.dart';
import 'package:community_app/common/widget_factory.dart';
import 'package:community_app/model/post.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:community_app/viewmodel/category_view_model.dart';
import 'package:community_app/viewmodel/post_view_model.dart';
import 'package:community_app/viewmodel/suburb_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class PostAdScreen extends StatefulWidget {
  @override
  PostAdScreenState createState() {
    return new PostAdScreenState();
  }
}

class PostAdScreenState extends State<PostAdScreen> implements PostListener {
  final TextEditingController moduleDescriptionController =
      new TextEditingController();
  final TextEditingController moduleTitleNameController =
      new TextEditingController();
  SharedPreferencesHelper sharedPreferencesHelper;
  User user;
  String x;
  String emailx;
  String namex;
  String phonex;
  String userIdx;

  PostViewModel addsViewModel;
  String _mySelection;
  String _serviceSelection;

  Future<File> _image;
  String base64Image;
  File tmpFile;
  String filename;
  String errMessage = 'Error uploading Image';

  @override
  void initState() {
    user = User();
    sharedPreferencesHelper = SharedPreferencesHelper();
    addsViewModel = PostViewModel();
    getUserId();
    super.initState();
  }

  Future _onGalleryPressed() async {
    setState(() {
      _image = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }

  Widget showImage() {
    return FutureBuilder<File>(
      future: _image,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            null != snapshot.data) {
          tmpFile = snapshot.data;
          base64Image = base64Encode(snapshot.data.readAsBytesSync());
          print(base64Image);
          filename = tmpFile.path.split('/').last;
          return Flexible(
            child: Image.file(
              snapshot.data,
              fit: BoxFit.fill,
            ),
          );
        } else if (null != snapshot.error) {
          return const Text(
            'Error picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return const Text('No Image Selected', textAlign: TextAlign.center);
        }
      },
    );
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    user = User().fromMap(jsonDecode(x));
    namex = user.name.toString();
    phonex = user.phoneNumber.toString();
    emailx = user.emailAddress.toString();
    userIdx = user.id.toString();
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Post Your Ad',
          style: Theme.of(context).textTheme.subhead,
          textAlign: TextAlign.center,
        ),
      ),
      body: new ListView(
        padding: EdgeInsets.all(20),
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              serviceDropDown(context),
              SizedBox(
                height: 10,
              ),
              createInputField(
                moduleTitleNameController,
                "Title",
                false,
              ),
              SizedBox(
                height: 10,
              ),
              createInputField(moduleDescriptionController, "Description", false),
              SizedBox(
                height: 10,
              ),
              suburbDropdown(context),
              SizedBox(
                height: 5,
              ),
              CustomButtonWhite(
                text: "Choose Image",
                onPressed: () {
                  _onGalleryPressed();
                },
              ),
              SizedBox(
                height: 5,
              ),
              showImage(),
              SizedBox(
                height: 5,
              ),
              CustomButton(
                text: "Post",
                onPressed: () {
                  addsViewModel.post(
                      Quote(
                        name: namex,
                        postTitle: moduleTitleNameController.text,
                        phoneNumber: phonex,
                        categoryId: _serviceSelection,
                        email: emailx,
                        suburb: _mySelection,
                        userId: userIdx,
                        postImageName: filename,
                        postDescription: moduleDescriptionController.text,
                        postCategoryName: _serviceSelection,
                        postFeaturedImage: base64Image,
                      ),
                      this);
                },
              )
            ],
          ),
        ],
      ),
    );
  }

  @override
  void onFail() {
    //todo  toast error
  }

  @override
  void onSuccess() {
    Navigator.of(context).pushNamed(HOME_SCREEN);
  }

  Widget suburbDropdown(BuildContext context) {
    Provider.of<SuburbViewModel>(context, listen: false).listSuburb();

    return Consumer<SuburbViewModel>(
        builder: (context, suburbViewModel, child) {
      return FormField(
        builder: (FormFieldState state) {
          return InputDecorator(
            decoration: InputDecoration(
              icon: const Icon(Icons.location_city),
              labelText: 'Suburb',
            ),
            child: new DropdownButtonHideUnderline(
              child: new DropdownButton(
                value: _mySelection,
                isDense: true,
                onChanged: (newVal) {
                  setState(() {
                    _mySelection = newVal;
                    state.didChange(newVal);
                  });
                },
                items: suburbViewModel.suburbList.map((item) {
                  return new DropdownMenuItem(
                    value: item.suburbName,
                    child: new Text(item.suburbName),
                  );
                }).toList(),
              ),
            ),
          );
        },
      );
    });
  }

  Widget serviceDropDown(BuildContext context) {
    Provider.of<CategoryViewModel>(context, listen: false).getAllCategories();

    return Consumer<CategoryViewModel>(
        builder: (context, categoryViewModel, child) {
      return FormField(
        builder: (FormFieldState state) {
          return InputDecorator(
            decoration: InputDecoration(
              icon: const Icon(Icons.open_with),
              labelText: 'Service',
            ),
            child: new DropdownButtonHideUnderline(
              child: new DropdownButton(
                value: _serviceSelection,
                isDense: true,
                onChanged: (newVal) {
                  setState(() {
                    _serviceSelection = newVal;
                    state.didChange(newVal);
                  });
                },
                items: categoryViewModel.categoryList.map((item) {
                  return new DropdownMenuItem(
                    value: item.id,
                    child: new Text(item.categoryName),
                  );
                }).toList(),
              ),
            ),
          );
        },
      );
    });
  }
}
