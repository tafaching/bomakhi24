import 'dart:convert';

import 'package:community_app/model/city.dart';
import 'package:community_app/model/suburb.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:flutter/material.dart';

class SuburbViewModel extends ChangeNotifier with ASuburbViewModel {
  String cityId = 'api/getSuburbByCityId/{cityId}';
  String allSuburb = 'api/allSuburbs';
  String createSuburb = "api/addSuburb";
  SharedPreferencesHelper sharedPreferencesHelper=SharedPreferencesHelper();
  List<Suburb> suburbList = [];

  @override
  Future getAllSuburbs() async {
    await GenericRepository<Suburb>()
        .getAll(allSuburb, Suburb(), null, null)
        .then((response) {
      suburbList = response;
    });
    notifyListeners();
  }



  @override
  void listSuburb() async {
    final String x = await SharedPreferencesHelper().getUser("USER");
    print("TAFADZWA" + x);
    User user = User().fromMap(jsonDecode(x));
    print("GUTA" + user.city);
    print(user);

    await GenericRepository<Suburb>().getAll(cityId, Suburb(), null,
        {'cityId': user.city}).then((response) {
      suburbList = response;
    });
    notifyListeners();
  }


  @override
  void addSuburb(Suburb suburb, SuburbListener suburbListener) async {
    await GenericRepository<Suburb>()
        .post(createSuburb, suburb, null, null)
        .then((response) {
      String groupJson = jsonEncode(suburb.toJson()).toString();
      print('Status Code: ' +
          response.statusCode.toString() +
          " groupJson" +
          groupJson);
      suburbListener.onSuccess();
    }).catchError((onError) {
      suburbListener.onFail();
    });
  }



}

abstract class SuburbListener {
  void onSuccess();

  void onFail();
}

abstract class ASuburbViewModel {
  void getAllSuburbs();
  void listSuburb();
  void addSuburb(Suburb city, SuburbListener cityListener);
}
