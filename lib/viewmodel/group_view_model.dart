import 'package:community_app/model/group.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class GroupViewModel extends ChangeNotifier with AGroupViewModel {
  String groups = 'api/getGroups/{suburbId}';

  List<Group> groupList = [];

  @override
  void getGroupsBySuburbId(String id) async {
    await GenericRepository<Group>()
        .getAll(groups, Group(), null, {'suburbId': id}).then((response) {
      groupList = response;
    });
    notifyListeners();
  }
}

abstract class AGroupViewModel {
  void getGroupsBySuburbId(String id);
}
