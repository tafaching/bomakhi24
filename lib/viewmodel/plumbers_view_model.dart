import 'package:community_app/model/post.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class PlumbersViewModel extends ChangeNotifier with APlumbersViewModel {
  String plumbersApi = 'api/categoryAdds/{id}';
  List<Quote> plumbersList = [];
  String id ="4";
  @override
  void getPlumbers() async {
    //get properties
    await GenericRepository<Quote>()
        .getAll(plumbersApi, Quote(), null, {'id': id}).then((response) {
      plumbersList = response;
    });
    notifyListeners();
  }
}

abstract class APlumbersViewModel {
  void getPlumbers();
}
