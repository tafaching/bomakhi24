import 'package:community_app/model/post.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class GardenViewModel extends ChangeNotifier with AGardenViewModel {
  String gardenApi = 'api/categoryAdds/{id}';
  List<Quote> gardenList = [];
  String id = "5";

  @override
  void getGarden() async {
    await GenericRepository<Quote>()
        .getAll(gardenApi, Quote(), null, {'id': id}).then((response) {
      gardenList = response;
    });
    notifyListeners();
  }
}

abstract class AGardenViewModel {
  void getGarden();
}
