import 'package:community_app/model/post.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class CarpentersViewModel extends ChangeNotifier with ACarpentersViewModel {
  String carpentersApi = 'api/categoryAdds/{id}';
  List<Quote> carpentersList = [];
  String id ="6";
  @override
  void getCarpenters() async {
    //get properties
    await GenericRepository<Quote>()
        .getAll(carpentersApi, Quote(), null, {'id': id}).then((response) {
      carpentersList = response;
    });
    notifyListeners();
  }
}

abstract class ACarpentersViewModel {
  void getCarpenters();
}
