import 'package:community_app/model/post.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class ElectricalViewModel extends ChangeNotifier with AElectricalViewModel {
  String electricalApi = 'api/categoryAdds/{id}';
  List<Quote> electricalList = [];


  @override
  void getElectrical(String id) async {
    await GenericRepository<Quote>()
        .getAll(electricalApi, Quote(), null, {'id': id}).then((response) {
      electricalList = response;
    });
    notifyListeners();
  }
}

abstract class AElectricalViewModel {
  void getElectrical(String id);
}
