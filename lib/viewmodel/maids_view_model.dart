import 'package:community_app/model/post.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class MaidsViewModel extends ChangeNotifier with AMaidsViewModel {
  String maidsApi = 'api/categoryAdds/{id}';
  List<Quote> maidsList = [];
  String id = "3";

  @override
  void getMaids() async {
    await GenericRepository<Quote>()
        .getAll(maidsApi, Quote(), null, {'id': id}).then((response) {
      maidsList = response;
    });
    notifyListeners();
  }
}

abstract class AMaidsViewModel {
  void getMaids();
}
