import 'dart:convert';

import 'package:community_app/model/user.dart';
import 'package:community_app/model/wish.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:flutter/material.dart';


class WishViewModel extends ChangeNotifier with ACategoryViewModel {
  String getWish = 'api/getWish/{id}';
  String addWish = 'api/addWish';
  String deleteItem = 'api/wish/{id}';
  Wish wish;
  String u;
  List<Wish> wishList = [];
  SharedPreferencesHelper sharedPreferencesHelper;

  @override
  void getWishByUserId() async {
    final String x = await SharedPreferencesHelper().getUser("USER");
    print("wish  IYO" + x);
    User user = User().fromMap(jsonDecode(x));
    u=user.id.toString();
    print("wish  ID" + user.id.toString());
    print(user);

    //TODO REMOVE MAGIC STRING USER ID

    await GenericRepository<Wish>()
        .getAll(getWish, Wish(), null, {'id': u}).then((response) {
      wishList = response;
    });
    notifyListeners();
  }

  @override
  void deleteItemById(String id) async {
    //get properties
    await GenericRepository<Wish>()
        .deleteAll(deleteItem, Wish(), null, {'id': id}).then((response) {});
    notifyListeners();
  }

  @override
  void addToWish(Wish wish) async {
    await GenericRepository<Wish>()
        .post(addWish, wish, null, null)
        .then((response) {})
        .catchError((onError) {
      notifyListeners();
    });
  }
}

abstract class ACategoryViewModel {
  void getWishByUserId();
  void deleteItemById(String id);
  void addToWish(Wish wish);

}
