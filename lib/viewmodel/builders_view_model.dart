import 'package:community_app/model/post.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class BuildersViewModel extends ChangeNotifier with ABuildersViewModel {
  String buildersApi = 'api/categoryAdds/{id}';
  List<Quote> buildersList = [];
  String id = "1";

  @override
  void getBuilders() async {
    //get properties
    await GenericRepository<Quote>()
        .getAll(buildersApi, Quote(), null, {'id': id}).then((response) {
      buildersList = response;
    });
    notifyListeners();
  }
}

abstract class ABuildersViewModel {
  void getBuilders();
}
