import 'dart:convert';

import 'package:community_app/model/post.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:flutter/material.dart';

class PostViewModel extends ChangeNotifier with APostViewModel {
  String postAd = "api/postAdd";
  String postByCategoryId = 'api/categoryAdds/{id}';
  String deleteMember = "api/group/{phoneNumber}";
  Quote posts = Quote();

  List<Quote> postList = [];

  @override
  void post(Quote emergency, PostListener emergencyListener) async {
    await GenericRepository<Quote>()
        .post(postAd, emergency, null, null)
        .then((response) {
      String groupJson = jsonEncode(emergency.toJson()).toString();
      print('Status Code: ' +
          response.statusCode.toString() +
          " groupJson" +
          groupJson);
      emergencyListener.onSuccess();
    }).catchError((onError) {
      emergencyListener.onFail();
    });
  }

  @override
  void getAdsByCatId(String id) async {
    //get properties
    await GenericRepository<Quote>()
        .getAll(postByCategoryId, Quote(), null, {'id': id}).then((response) {
      postList = response;
    });
    notifyListeners();
  }

  @override
  void deleteContactByPhoneNumber() async {
    final String x = await SharedPreferencesHelper().getUser("USER");

    Quote emergency = Quote().fromMap(jsonDecode(x));
    await GenericRepository<Quote>().deleteAll(deleteMember, Quote(), null,
        {'phoneNumber': emergency.phoneNumber}).then((response) {});
    notifyListeners();
  }
}

abstract class DeleteListener {
  void onSuccess();

  void onFail();
}

abstract class PostListener {
  void onSuccess();

  void onFail();
}

abstract class APostViewModel {
  void deleteContactByPhoneNumber();
  void getAdsByCatId(String id);

  void post(Quote emergency, PostListener emergencyListener);
}
