import 'dart:convert';

import 'package:community_app/model/user.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:flutter/material.dart';

class RegistrationViewModel extends ChangeNotifier with ARegisterViewModel {
  String loginPoint = 'api/register';
  User x = User();
  SharedPreferencesHelper sharedPreferencesHelper=SharedPreferencesHelper();


  @override
  void register(User user, RegistrationListener registrationListener) async {
    String userJson;
    await GenericRepository<User>()
        .postResponseList(loginPoint, user, null, null)
        .then((response) async {
      userJson = jsonEncode(response[0].toJson());
      print(userJson);
      print("TAFADZWA" + userJson);
    }).catchError((onError) {
      registrationListener.onFail();
    });

    await sharedPreferencesHelper.saveUser("USER", userJson).then((isSaved) {
      if (isSaved) {

        print("succefully saved" + userJson);
        registrationListener.onSuccess();
      } else {
        print("not saved" + userJson);
      }
    }).catchError((onError) {
      print("oops something went wrong" + onError);
    });
  }
}

abstract class RegistrationListener {
  void onSuccess();

  void onFail();
}

abstract class ARegisterViewModel {
  void register(User user, RegistrationListener registrationListener);
}
