import 'dart:convert';

import 'package:community_app/model/categories.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class CategoryViewModel extends ChangeNotifier with ACategoryViewModel {
  String categoryId = 'api/category/{id}';
  String allCategories = 'api/allCategories';
  String createCategory = "api/postCategory";

  List<Categories> categoryList = [];

  @override
  Future getAllCategories() async {
    await GenericRepository<Categories>()
        .getAll(allCategories, Categories(), null, null)
        .then((response) {
      categoryList = response;
    });
    notifyListeners();
  }

  @override
  void getCategoryById(String id) async {
    await GenericRepository<Categories>().getAll(
        categoryId, Categories(), null, {'id': id}).then((response) {
      categoryList = response;
    });
    notifyListeners();
  }

  @override
  void addCategories(Categories category, CategoryListener categoryListener) async {
    await GenericRepository<Categories>()
        .post(createCategory, category, null, null)
        .then((response) {
      String groupJson = jsonEncode(category.toJson()).toString();
      print('Status Code: ' +
          response.statusCode.toString() +
          " groupJson" +
          groupJson);
      categoryListener.onSuccess();
    }).catchError((onError) {
      categoryListener.onFail();
    });
  }
}

abstract class CategoryListener {
  void onSuccess();

  void onFail();
}

abstract class ACategoryViewModel {
  void getAllCategories();

  void getCategoryById(String id);

  void addCategories(Categories category, CategoryListener cityListener);
}
