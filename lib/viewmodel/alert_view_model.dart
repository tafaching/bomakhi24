
import 'package:community_app/model/alert.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/cupertino.dart';

class AlertViewModel extends ChangeNotifier with AAlertViewModel {

  String alertContacts = 'assets/alert_list/alertList.json';


  List<Alert> alertList = [];

  @override
  Future getAllAlertContacts() async {

    await GenericRepository<Alert>()
        .getAll(alertContacts, Alert(), null, null)
        .then((response) {
      alertList = response;
    });
    notifyListeners();
  }

}

abstract class AAlertViewModel {
  void getAllAlertContacts();

}
