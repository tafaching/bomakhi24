import 'dart:convert';

import 'package:community_app/model/city.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:flutter/material.dart';

class CityViewModel extends ChangeNotifier with ACityViewModel {
  String cityId = 'api/cityId/{cityId}';
  String allCities = 'api/cities';
  String createCity = "api/addCity";

  List<Categories> cityList = [];

  @override
  Future getAllCities() async {
    await GenericRepository<Categories>()
        .getAll(allCities, Categories(), null, null)
        .then((response) {
      cityList = response;
    });
    notifyListeners();
  }

  @override
  void getCityById(String id) async {
    await GenericRepository<Categories>()
        .getAll(cityId, Categories(), null, {'cityId': id}).then((response) {
      cityList = response;
    });
    notifyListeners();
  }

  @override
  void addCity(Categories city, CityListener cityListener) async {
    await GenericRepository<Categories>()
        .post(createCity, city, null, null)
        .then((response) {
      String groupJson = jsonEncode(city.toJson()).toString();
      print('Status Code: ' +
          response.statusCode.toString() +
          " groupJson" +
          groupJson);
      cityListener.onSuccess();
    }).catchError((onError) {
      cityListener.onFail();
    });
  }
}

abstract class CityListener {
  void onSuccess();

  void onFail();
}

abstract class ACityViewModel {
  void getAllCities();

  void getCityById(String id);

  void addCity(Categories city, CityListener cityListener);
}
