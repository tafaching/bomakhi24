import 'dart:convert';

import 'package:community_app/model/emergency.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/repository/generic_repository.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:flutter/material.dart';

class EmergencyViewModel extends ChangeNotifier with AEmergencyViewModel {
  String allContacts = 'api/myEmergencyContactsByPhone/{adminPhoneNumber}';
  String myGroup = 'api/myGroupByUserId/{userId}';
  String addContact = "api/addEmergencyContacts";
  String familyPanicEndPoint = "api/sendEmergencyMessage/{adminPhoneNumber}";
  String communityPanicEndPoint = "api/sendCommunityEmergencyMessage/{groupName}";
  String deleteMember = "api/group/{phoneNumber}";
  SharedPreferencesHelper sharedPreferencesHelper = SharedPreferencesHelper();

  Emergency emergency = Emergency();

  List<Emergency> emergencyList = [];

  @override
  void getEmergencyContacts(String id) async {
    await GenericRepository<Emergency>()
        .getAll(allContacts, Emergency(), null, {'adminPhoneNumber': id}).then(
            (response) {
      emergencyList = response;
    });
    notifyListeners();
  }

  @override
  void addEmergencyContacts(
      Emergency emergency, EmergencyListener emergencyListener) async {
    await GenericRepository<Emergency>()
        .post(addContact, emergency, null, null)
        .then((response) {
      String groupJson = jsonEncode(emergency.toJson()).toString();
      print('Status Code: ' +
          response.statusCode.toString() +
          " groupJson" +
          groupJson);
      emergencyListener.onSuccess();
    }).catchError((onError) {
      emergencyListener.onFail();
    });
  }

  @override
  void getMembershipByPhone() async {
    final String x = await SharedPreferencesHelper().getUser("USER");
    User user = User().fromMap(jsonDecode(x));
    await GenericRepository<Emergency>().getAll(allContacts, Emergency(), null,
        {'adminPhoneNumber': user.phoneNumber.toString()}).then((response) {
      emergencyList = response;
    });
    notifyListeners();
  }

  @override
  void getMembershipByUserId() async {
    String memberJson;
    final String x = await SharedPreferencesHelper().getUser("USER");
    print("NDIVHUWO" + x);
    User user = User().fromMap(jsonDecode(x));
    print("TAFADZWA" + user.id);
    print(user);

    await GenericRepository<Emergency>().getAll(
        myGroup, Emergency(), null, {'userId': user.id}).then((response) async {
      memberJson = jsonEncode(response[0].toJson());
      print(memberJson);
      print("MONICA MEMBER" + memberJson);
    }).catchError((onError) {});
    await sharedPreferencesHelper
        .saveGroupName("MEMBER", memberJson)
        .then((isSaved) {
      if (isSaved) {
        print("succefully saved" + memberJson);
      } else {
        print("not saved" + memberJson);
      }
    }).catchError((onError) {
      print("oops something went wrong" + onError);
    });

    notifyListeners();
  }

  @override
  void familyPanic() async {
    final String x = await SharedPreferencesHelper().getUser("USER");
    print("USER" + x);
    User user = User().fromMap(jsonDecode(x));
    print("PHONE" + user.phoneNumber);
    print(user);

    await GenericRepository<Emergency>().getAll(
        familyPanicEndPoint,
        Emergency(),
        null,
        {'adminPhoneNumber': user.phoneNumber.toString()}).then((response) {
      emergencyList = response;
    });
    notifyListeners();
  }

  @override
  void communityPanic() async {
    String groupName_from_pref;
    final String x = await SharedPreferencesHelper().getGroupName("MEMBER");
    print("MEMBER" + x);
    Emergency emergency = Emergency().fromMap(jsonDecode(x));
    print("GROUP NAME" + emergency.groupName);
    print(emergency);

    await GenericRepository<Emergency>().getSingle(
        communityPanicEndPoint,
        Emergency(),
        null,
        {'groupName': emergency.groupName}).then((response) async {
      groupName_from_pref = jsonEncode(response[0].toJson());
      print(groupName_from_pref);
      print("MONICA MEMBER" + groupName_from_pref);
    }).catchError((onError) {});
    notifyListeners();
  }

  @override
  void deleteContactByPhoneNumber() async {
    final String x = await SharedPreferencesHelper().getUser("USER");

    Emergency emergency = Emergency().fromMap(jsonDecode(x));
    await GenericRepository<Emergency>().deleteAll(deleteMember, Emergency(),
        null, {'phoneNumber': emergency.phoneNumber}).then((response) {});
    notifyListeners();
  }
}

abstract class DeleteListener {
  void onSuccess();

  void onFail();
}

abstract class EmergencyListener {
  void onSuccess();

  void onFail();
}

abstract class AEmergencyViewModel {
  void getEmergencyContacts(String id);

  void getMembershipByPhone();

  void getMembershipByUserId();

  void deleteContactByPhoneNumber();

  void familyPanic();

  void communityPanic();

  void addEmergencyContacts(
      Emergency emergency, EmergencyListener emergencyListener);
}
