import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/viewmodel/alert_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../main.dart';

class AlertEmergency extends StatefulWidget {
  @override
  AlertEmergencyState createState() {
    return new AlertEmergencyState();
  }
}

class AlertEmergencyState extends State<AlertEmergency> {
  final CallsAndMessagesService _service = locator<CallsAndMessagesService>();

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    Provider.of<AlertViewModel>(context, listen: false).getAllAlertContacts();
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
          title: Text(
            "Emergency Contacts",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          iconTheme: new IconThemeData(color: Colors.white)),
      floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.black,
            size: 20,
          ),
          backgroundColor: Colors.white,
          onPressed: () {
            Navigator.of(context).pushNamed(ADD_EMERGENCY_CONTACTS);
          }),
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/images/community_app.jpeg"),
                fit: BoxFit.fill,
              ),
            ),
          ),
//          _buildTopBar(),
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Builder(
      builder: (BuildContext context) {
        return Positioned(
          child: Container(
            padding: const EdgeInsets.only(
                left: 0.0, top: 16.0, bottom: 16.0, right: 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Consumer<AlertViewModel>(
                      builder: (context, alertViewModel, child) {
                    return ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: alertViewModel.alertList.length,
                        itemBuilder: (BuildContext c, int index) {
                          final item = alertViewModel.alertList[index];

                          return Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey[100],
                                      spreadRadius: 10.0,
                                      blurRadius: 4.5)
                                ]),
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 4),
                            margin: EdgeInsets.symmetric(
                                horizontal: 32, vertical: 20),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 8, right: 16),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 16.0),
                                          child: Text(
                                            item.name,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18,
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 80.0, right: 6.0),
                                          child: IconButton(
                                            icon: new Icon(Icons.call),
                                            highlightColor: Colors.green,
                                            color: Colors.green,
                                            onPressed: () =>
                                                _service.call(item.phoneNumber),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        });
                  }),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void onFail() {
    // TODO: implement onFail
  }

  @override
  void onSuccess() {
    Navigator.of(context).pushNamed(LIST_EMERGENCY);
  }
}

class CallsAndMessagesService {
  void call(String number) => launch("tel:$number");

  void sendSms(String number) => launch("sms:$number");

  void sendEmail(String email) => launch("mailto:$email");

  void  url(String url)  =>  launch("urlString:$url");
}
