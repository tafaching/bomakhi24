import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/Home/home.dart';
import 'package:community_app/common/custom_button.dart';
import 'package:community_app/viewmodel/emergency_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CommunityGroupListPage extends StatefulWidget {
  @override
  CommunityGroupListPageState createState() {
    return new CommunityGroupListPageState();
  }
}

class CommunityGroupListPageState extends State<CommunityGroupListPage> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    Provider.of<EmergencyViewModel>(context, listen: false)
        .getMembershipByPhone();
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (c) => Home(),
            ));
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Community Group ',
          style: Theme.of(context).textTheme.subhead,
          textAlign: TextAlign.center,
        ),
        centerTitle: true,
      ),
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
//          _buildTopBar(),
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Builder(
      builder: (BuildContext context) {
        return Positioned(
          child: Container(
            padding: const EdgeInsets.only(
                left: 0.0, top: 16.0, bottom: 16.0, right: 0.0),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Consumer<EmergencyViewModel>(
                      builder: (context, groupViewModel, child) {
                    List x = groupViewModel.emergencyList;
                    if (x.isEmpty) {
                      return Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        margin:
                            EdgeInsets.symmetric(horizontal: 32, vertical: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Welcome to Makhi community response system. Be your neighbor's keeper.\n\n",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 16),
                              textAlign: TextAlign.justify,
                            ),
                            Text(
                              "1. \t Click add to add family member or community patrol team members. You can ONLY five people for free.",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                              textAlign: TextAlign.justify,
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            Text(
                              "2. \t you have two free panics to test the system, playing with panic button will result in you banned from using the service for a month",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                              textAlign: TextAlign.justify,
                            ),
                            CustomButton(
                              text: "Add",
                              onPressed: () {
                                Navigator.of(context)
                                    .pushNamed(ADD_EMERGENCY_CONTACTS);
                              },
                            )
                          ],
                        ),
                      );
                    } else {
                      return ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: groupViewModel.emergencyList.length,
                          itemBuilder: (BuildContext c, int index) {
                            final item = groupViewModel.emergencyList[index];

                            return InkWell(
                                onTap: () {
                                  _showDialog(context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black12,
                                            spreadRadius: 10.0,
                                            blurRadius: 4.5)
                                      ]),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 4),
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 32, vertical: 20),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8, right: 16),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.person,
                                              color: Colors.black,
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 16.0),
                                                child: Text(
                                                  item.name,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 18,
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                            ),

                                            // Platform.isIOS
                                            //     ? const Text('Continue in iOS view')
                                            //     : const Text('Continue in Android view'),
                                            Expanded(
                                                child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 80.0,
                                                            right: 6.0),
                                                    child: (item.accepted == '1')
                                                        ? const Text(
                                                            'Accepted',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .green),
                                                          )
                                                        : const Text(
                                                            'Not Accepted',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .red)))),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ));
                          });
                    }
                  }),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _showDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text('Are sure you want to delete panic contact?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(HOME_SCREEN);
                },
                child: Text('cancel'),
              ),
              FlatButton(
                onPressed: () {
                  Provider.of<EmergencyViewModel>(context, listen: false)
                      .deleteContactByPhoneNumber();
                  Navigator.of(context).pop();
                },
                child: Text('yes'),
              )
            ],
          );
        });
  }
}
