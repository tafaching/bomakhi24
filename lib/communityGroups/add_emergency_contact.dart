import 'dart:convert';

import 'package:community_app/Constant/Constant.dart';
import 'package:community_app/common/custom_button.dart';
import 'package:community_app/common/widget_factory.dart';
import 'package:community_app/model/emergency.dart';
import 'package:community_app/model/user.dart';
import 'package:community_app/sharedPref/shared_preferences_helper.dart';
import 'package:community_app/viewmodel/emergency_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class EmergencyContact extends StatefulWidget {
  @override
  CreatePublicGroupState createState() {
    return new CreatePublicGroupState();
  }
}

class CreatePublicGroupState extends State<EmergencyContact>
    implements EmergencyListener {
  EmergencyViewModel emergencyViewModel;
  final TextEditingController moduleNameController =
      new TextEditingController();
  final TextEditingController modulePhoneNumberController =
      new TextEditingController();
  SharedPreferencesHelper sharedPreferencesHelper;
  User user;
  String x;
  String namex;
  String phonex;

  @override
  void initState() {
    super.initState();
    user = User();
    sharedPreferencesHelper = SharedPreferencesHelper();
    emergencyViewModel = EmergencyViewModel();
    getUserId();
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    user = User().fromMap(jsonDecode(x));
    namex = user.name.toString();
    phonex = user.phoneNumber.toString();
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushNamed(LIST_COMUNITY);
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          'Add Family Contact',
          style: Theme.of(context).textTheme.subhead,
          textAlign: TextAlign.center,
        ),centerTitle: true,
      ),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
//TODO : ADD multiple contacts from phone book
        //TODO ADD from phonebook

        child: new Center(
            child: new Form(
          child: new ListView(
            children: <Widget>[
              createInputField(moduleNameController, "eg Thomas", false),
              SizedBox(
                height: 16,
              ),
              createInputField(
                  modulePhoneNumberController, "eg 073 88837905", false),
              CustomButton(
                text: "Create",
                onPressed: () {
                  emergencyViewModel.addEmergencyContacts(
                      Emergency(
                        name: moduleNameController.text,
                        phoneNumber:
                            validateMobile(modulePhoneNumberController.text),
                        adminPhoneNumber: phonex,
                        accepted: '0'
                      ),
                      this);
                },
              )
            ],
          ),
        )),
      ),
    );
  }

  String validateMobile(String value) {
    var re = RegExp(r'\d{1}');

    String str = value.replaceFirst(re, "+27");

    if (str.length == 0) {
      return 'Please enter mobile number';
    }
    return str;
  }

  @override
  void onFail() {
    // TODO: implement onFail
  }

  @override
  void onSuccess() {
    //TODO : toast implemenatation
    Navigator.of(context).pushNamed(LIST_COMUNITY);
  }
}
