import 'package:community_app/model/base_model.dart';
import 'package:community_app/model/json_factory.dart';
import 'package:community_app/remote/http_remote_service.dart';

class GenericRepository<T extends ModelMapper> {
  Future<List<T>> getAll(
      String endPoint, T responseType, Map queryParams, Map pathVariables) {
    return getData(buildUrl(endPoint, pathVariables), params: queryParams)
        .then((response) {
      return JsonEncoderMapper<T>().mapToLis(responseType, response);
    });
  }

  Future<T> deleteAll(
      String endPoint, T responseType, Map queryParams, Map pathVariables) {
    return deleteData(buildUrl(endPoint, pathVariables))
        .then((response) {
      return responseType.fromMap(response);
    }).catchError((onError) {});
  }

  Future<T> getSingleItem(
      String endPoint, T responseType, Map queryParams, Map pathVariables) {
    return getData(buildUrl(endPoint, pathVariables)).then((response) {
      return responseType.fromMap(response);
    }).catchError((onError) {});
  }

  Future<List<T>> getSingle(
      String endPoint, T responseType, Map queryParams, Map pathVariables) {
    return getData(buildUrl(endPoint, pathVariables), params: queryParams)
        .then((response) {
      return JsonEncoderMapper<T>().mapToLis(responseType, response);
    });
  }


  Future post(String endPoint, T request, Map queryParams, Map pathVariables) {
    return postData(buildUrl(endPoint, pathVariables), request.toJson())
        .then((response) {
      return response;
      //Deserialize response
    }).catchError((onError) {
      //do something
    });
  }

  Future put(String endPoint, T request, Map queryParams, Map pathVariables) {
    return putData(buildUrl(endPoint, pathVariables), request.toJson())
        .then((response) {
      return response;
      //Deserialize response
    }).catchError((onError) {
      //do something
    });
  }

  Future<T> postResponse(
      String endPoint, T request, Map queryParams, Map pathVariables) {
    return postData(buildUrl(endPoint, pathVariables), request.toJson())
        .then((response) {
      return request.newInstance().fromMap(response.data);
      //Deserialize response
    }).catchError((onError) {
      //do something
    });
  }

  Future<List <T>> postResponseList(
      String endPoint, T request, Map queryParams, Map pathVariables) {
    return postData(buildUrl(endPoint, pathVariables), request.toJson())
        .then((response) {
      return JsonEncoderMapper<T>().mapToLis(request, response.data);
    }).catchError((onError) {
      //do something
    });
  }


  buildUrl(String endPoint, Map pathVariables) {
    if (pathVariables != null) {
      for (MapEntry mapEntry in pathVariables.entries) {
        endPoint = endPoint.replaceAll(
            '{' + mapEntry.key + '}', mapEntry.value.toString());
      }
    }
    return endPoint;
  }
}
