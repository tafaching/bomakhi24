import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  Future<bool> saveUser(String key, String body) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, body);
  }

  Future<bool> saveUserOne(String key, String body) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, body);
  }

  Future<String> getUser(key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<bool> saveGroupName(String key, String body) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, body);
  }

  Future<String> getGroupName(key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
}
